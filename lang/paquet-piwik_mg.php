<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-piwik?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'piwik_description' => 'Ajoute le script de statistiques Piwik (analyseur de trafic web) sur les pages du site.',
	'piwik_slogan' => 'Interfacer Spip et Piwik'
);
