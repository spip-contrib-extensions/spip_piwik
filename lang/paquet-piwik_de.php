<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-piwik?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'piwik_description' => 'Fügt den Webseiten das Statistiktool (Website Traffic Analyse) PIWIK hinzu.',
	'piwik_slogan' => 'SPIP-Schnittstelle mit PIWIK'
);
