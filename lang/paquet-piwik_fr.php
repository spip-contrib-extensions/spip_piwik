<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/spip_piwik.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'piwik_description' => 'Ajoute le script de statistiques Piwik (analyseur de trafic web) sur les pages du site.',
	'piwik_slogan' => 'Interfacer Spip et Piwik'
);
