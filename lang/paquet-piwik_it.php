<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-piwik?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'piwik_description' => 'Aggiunge lo script di statistiche Piwik (analizzatore di traffico web) alle pagine del sito.',
	'piwik_slogan' => 'Interfacciamento tra Spip e Piwik'
);
